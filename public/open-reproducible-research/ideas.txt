Why you will never write good code
----------------------------------

*** Humans making mistakes is an invariant
    * All software has bugs - but how much?
        * https://www.mayerdan.com/ruby/2012/11/11/bugs-per-line-of-code-ratio
        * https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4629271/pdf/f1000research-3-7365.pdf
        * https://it.slashdot.org/story/13/05/07/1242244/450-million-lines-of-code-cant-be-wrong-how-open-source-stacks-up
        * https://labs.sogeti.com/how-many-defects-are-too-many/
        * Linux - https://www.govtech.com/security/Four-Year-Analysis-Finds-Linux-Kernel-Quality.html
        * https://www.fastcompany.com/28121/they-write-right-stuff (space shuttle)
        * (1 defect per 1000, flight software) https://www.nasa.gov/pdf/418878main_FSWC_Final_Report.pdf - 80% functionality in F-22, 2.5 million lines - 2,500 bugs.
        * https://www.nature.com/articles/d41586-018-05256-0

	* Time and cost not controlled or estimated - but research software should be more focussed on getting it right. Bugs mean inconsistent, unreliable, wrong software.
	* How often do you see single author papers? Why?
    * Defects not just in your code, but libraries, tools etc.


Culture/Environment
-------

Peers
-----
* Competitiveness is slowly slipping from code fragments written to do a bit of science (writing throwaway things to 'get a job done'), to software engineering - long lasting code, reused, published, shared.
* software collaboration is not the same as academic collaboration


*** Collaboration gives a chance to introduce software engineering.
    * What is a bug? Not working as it should, misunderstanding of how it should work, 
    * Quality -> Zen motorcycle maint.
    * Software carpentry.

* To make your code better:
	* consistent coding style (use one that can be run through a static checker)
	* Other static checkers
	* Test as often as possible, break noisily etc.
    * CI/CD pipelines
	* small steps

*** Data science bugs - buggy data - cleaning/QC issues and errors - automate everything!

	* Collaboration!
		* Code reviews (papers peer reviewed - code never is?)
		* Don't reinvent the wheel / do it all yourself
		* Version control
		* issue tracker
		* Readability - most mistakes caused by human error. Code read far more times than it's written.
		* Don't own your code, be egoless. Code is like a paper you can publish up to several times a day.

Unix philosophy:
	* Write small programs that do one thing, and does it well
	* Text outputs
	* etc...

*** To start collaborating, learn version control, git. and use it - the great enabler

* It's the process that writes good software, not the people

https://blog.codinghorror.com/in-programming-one-is-the-loneliest-number/
	* What good are nifty coding tricks if you can't show them off to anyone? How can you possibly learn the craft without being exposed to other programmers with different ideas, different approaches, and different skillsets? Who will review your code and tell you when there's an easier approach you didn't see? If you're serious about programming, you should demand to work with your peers.

* Open source has won - smug mode - get ahead in advertising
    * Not because of spending vast amounts of money, or monopolising sales channels
    * Best practices, code not tied to an entity with a limited life (companies) or supported by money
    * Based on a wealth of proven, winning tactics and collaboration / openness

