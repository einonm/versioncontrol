# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     cell_metadata_json: true
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.5.2
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# <h1><center>DPMCN Home Day 2020<center></h1>
# <h1><center>Open Reproducible Research</center></h1>
# <h1><center><a href="http://einon.net/open-reproducible-research">einon.net/open-reproducible-research</a></center></h1>
# <h3><center>Mark Einon, HPC Systems Manager</center></h3>
# <h3><center>MRC Centre for Neuropsychiatric Genetics &amp; Genomics</center></h3>
# <h3><center>einonm@cardiff.ac.uk</center></h3>

# + {"slideshow": {"slide_type": "-"}, "active": ""}
# <script>
#   $( document ).ready(function(){
#     <!--$('div.input').hide()-->
#     $('div.prompt').hide()
#   });
#   
# </script>

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # What is Open Reproducible Research?
#
# <img src="./Os_taxonomy.png" style="float: centre; width: 10260px; height: 540px;">
#
# <small>https://www.fosteropenscience.eu</small>

# + [markdown] {"slideshow": {"slide_type": "notes"}}
# Part of the Open Science (the 'open' should be redundant, but isn't - large parts of science are not open). 

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # What is Open Reproducible Research? (Cont.)
#
# Open science involves making scientific methods, data, and outcomes available to everyone. It can be broken down into several parts, including:
#
# * Transparency in data collection, processing and analysis methods, and derivation of outcomes.
# * Publicly available data and associated processing methods.
# * Transparent communication of results..
#
# Reproducible science is when anyone (including others and your future self) can understand and replicate the steps of an analysis, applied to the same or even new data - achieving the same results.
#
# Together, open reproducible science results from open workflows that allow you to easily share work and collaborate with others as well as openly publish your data and workflows to contribute to greater scientific knowledge.
#
#
# <small> From https://www.earthdatascience.org/courses/intro-to-earth-data-science/open-reproducible-science/get-started-open-reproducible-science/</small><br>
# <small>Also see https://lgatto.github.io/open-and-rr-2/</small>

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Why is open reproducible research important to us?
# ## The story so far...

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * For a long time, it has been possible to be a top researcher in the field without ever having shared working, reliable code.

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Funders would still give you money

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Publishers would still publish without working code

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Peer reviewers would not insist on having verifiable code

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Unlikely that anyone would ask to repeat your findings using your scripts (also easy to fob off)

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * PhDs happen without this sort of collaboration - the code probably wasn't reviewed, shared or run by anyone else, so the relevant collaboration skills are not taught.

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Why is open reproducible research important to us? (Cont.)
# ## The situation is changing fast...
#
# Researchers are being increasingly challenged to practice open science that goes beyond open access and open data, extending all the way to making software and data research outputs truly repeatable and reproducible.
#
# Funders[1], journals[2] and REF[3] are making strong recommendations and sometimes mandatory policy to instil an open science culture and adopt practices successfully established in open source communities, and the rate of such activities is increasing.
#
# These practices aim to ensure defect-free behaviour of software which is understandable by other researchers and produce results using repeatable pipelines.
#
# <small>1. https://wellcome.org/grant-funding/guidance/how-complete-outputs-management-plan</small><br>
# <small>2. https://www.nature.com/documents/GuidelinesCodePublication.pdf</small><br>
# <small>3. https://www.ukri.org/our-work/supporting-healthy-research-and-innovation-culture/open-research/</small>

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Why is open reproducible research being encouraged?
#
# * A **replication crisis** has been found to exist and open science is seen as a solution, of which open reproducible research is a part.
#
#
# * "*A 2016 poll of 1,500 scientists reported that 70% of them had failed to reproduce at least one other scientist's experiment (50% had failed to reproduce one of their own experiments). In 2009, 2% of scientists admitted to falsifying studies at least once and 14% admitted to personally knowing someone who did...*"[1]
#
#
# * Many findings were found to be doubtful [1], due to: 
#     * Generation of new data/publications at an unprecedented rate.
#     * Compelling evidence that the majority of these discoveries will not stand the test of time.
#     * **Causes: failure to adhere to good scientific practice and the desperation to publish or perish**
#     * This is a multifaceted, multi-stakeholder problem.
#     * No single party is solely responsible, and no single solution will suffice.
#     
# <small>1. https://en.wikipedia.org/wiki/Replication_crisis</small>

# + [markdown] {"slideshow": {"slide_type": "notes"}}
# Wasted effort, resources, money with little or no advancement of knowledge.

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Fixing the replication crisis is hindered by...
#
# * Not sharing the complete source code for analyses
# * A lack of information on how to use the software & codes
# * A high proportion of code bugs
# * Absence of reproducible methods & materials also creates a more fundamental **reproduction crisis**, where even given the published methods & data, results cannot be consistently reproduced, let alone replicated.
#
# **Open reproducible research addresses these issues**

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # All about bugs...
#
# * A bug is unexpected or incorrect behaviour
#
# <img src="./cc_bug.jpg" style="float: left; width: 200px; height: 200px;">
# <img src="./cc_bug2.jpg" style="float: left; width: 200px; height: 200px;">
# <img src="./inquisition.webp" style="float: left; width: 260px; height: 200px;">
# <img src="./cc_bug4.jpg" style="float: left; width: 200px; height: 200px;">

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## Software bugs
#
# * Rear Admiral Grace Hopper is noted to have popularised the term, having made a note about a real bug causing issues in an early computer, the Harvard Mark II, in 1946.

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# <img src="./first_bug.jpg" style="display: block; margin-left: auto; margin-right: auto; width: 460px;">

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * (Although the term 'bug' referring to a technical glitch has been around at least since Thomas Edison's time / 1870s)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ### Rear Admiral Grace Hopper
#
# <img src="./grace2.jpg" style="display: block; margin-left: auto; margin-right: auto; width: 600px">

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * She also invented the first compiler, contributing to the creation of the COBOL language

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * The phrase 'It’s easier to ask forgiveness than to get permission' is often attributed to her

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # For example, bugs can be subtle...
#
# #### A programmer is going to the grocery store, so their partner asks,
#
# ### "Buy a pint of milk, and if there are eggs, buy a dozen." 

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# So the programmer goes, buys everything, and drives back to the house.

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# Upon arrival, the partner angrily asks them,
#
# ##### "Why did you get 13 pints of milk?" 

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# The programmer says, 
#
# ## "There were eggs!"

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Buggy code - it's not you, it's us 

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Humans are very good at making such mistakes

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Developing code is a complex process, so humans make lots of mistakes
#
# <img src="./bobross.jpg" style="display: block; margin-left: auto; margin-right: auto; width: 300px">

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * **Bugs being put into code is an invariant**
#
# * Even with the current best practices - we don't have a way of changing this and probably never will

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## But what about the professionals? I bet NASA doesn't release buggy code...

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Yes, it does! They're acutely aware of it and NASA attempts to monitor defect rates closely (defects = bugs)

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * NASA acknowledged a defect rate of ~ 1 in every 1000 lines of code for flight software in 2009 [1] 
#
# <img src="./nasa-report.png" style="display: block; margin-left: auto; margin-right: auto; width: 700px">
#
# <small> 1. https://www.nasa.gov/pdf/418878main_FSWC_Final_Report.pdf </small>

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## Catch-22
#
# * Take the **Lockheed Martin F-22 Raptor** for example:
#     * 2.5 million lines of code
#     * 80% of functionality controlled by software...
#
# <img src="./f22.jpg" style="display: block; margin-left: auto; margin-right: auto; width: 400px">

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * **Estimated to have ~2,500 bugs**

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## Shuttle bug
#
# * Space shuttle Primary Avionics Systems Software (PASS) had 420,000 lines of code, with only 1 bug present in the last version (0.002 defects/KLOC)
# <img src="./shuttle.jpg" style="display: block; margin-left: auto; margin-right: auto; width: 400px"> 

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * NASA spent ~20 times more per line of code than average to get this ($1/2 billion)
#

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * The shuttle software engineers removed **99.9% of bugs prior to use**
#     * **These bugs were still present in the code before they were removed!**

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## So where does that leave everyone else?
#
# * Industry average is estimated to be **15-50 defects** per 1000 lines of code [1]

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Microsoft apps estimated to have **10-20 defects** per 1000 lines before QC, **0.5 defects/KLOC** on release (high quality commercial) [1]
#     * Using experienced, professional software engineers, following best practice
#     

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Linux OS measured to have **0.17 defects** per 1000 lines (best open source) [2]
#     * Mixture of hobbyist and professional software engineers
#     * Success relies heavily on the processes employed to develop the code, a.k.a. open source best practices
#     * Linus' Law: **'given enough eyeballs, all bugs are shallow'**
#
# <small>1. https://www.mayerdan.com/ruby/2012/11/11/bugs-per-line-of-code-ratio</small>
# <br><small>2. https://www.helpnetsecurity.com/2013/05/07/analyzing-450-million-lines-of-software-code/ </small>

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Software engineering

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * It's the application of systematic approaches to developing software

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Term first coined by Margaret Hamilton, while leading the software effort on the Apollo missions 
# <img src="./hamilton1.jpg" style="display: block; margin-left: auto; margin-right: auto; width: 200px"> 

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * **In an attempt to get software practices taken more seriously!**

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Covers: requirements, design, writing, testing, deployment, documentation... all the activities that can affect code quality

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## So, this 'software engineering' thing sounds good...

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * The majority of software engineering best practices are collaborative
#     * Peer reviews, documentation, readability/coding style, version control, issue tracking...    

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Many techniques rely on adopting particular collaborative working cultures
#     * Reviewing, code reuse, automation, requirements capture...

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * For a sole developer, many of the software engineering techniques are out of reach

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# *  **It's the process that creates good software, not the people**

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * We could say **if software development is the art of putting bugs into code, software engineering is the art of taking the bugs out**

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Why could research code be buggy and of poor quality?
# ### Bioinformaticians - software developers, or software engineers?
#
# * Many bioinformaticians are experienced and competent at writing code. But...

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Do bioinformaticians code alone ? 

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# <img src="./homealone.jpg" style="display: block; margin-left: auto; margin-right: auto; width: 260px">

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ### Bioinformaticians - software developers, or software engineers? (Cont.)
#
# * Is code peer reviewed?

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Is it common for each project to have a single bioinformatician working in isolation?

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Are tests written, available & run with known data inputs having known outputs?

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Are code & tests under version control?

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Is code shared with local peers & between projects?

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Are development processes understood, documented & shared?

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * If a bioinformatician moves jobs, does the knowledge they generated move with them?

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## Common excuses for not sharing code


# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * My code because it isn't good enough
#     * It won't get any better until others can see and use it
#     * If it's good enough to do a job, it's good enough to share 


# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * I won't be able to cope with the support requests
#     * Release and see what happens! Usually great effort is required to get your code noticed and used by others
#     * You don't have to support it


# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Someone will steal my awesome code / not cite it
#     * Code written by one developer is probably buggy and too specific for other use cases
#     * Code citations - do you cite all the code you use?


# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * It takes too long to clean up and document for release
#     * Best practice should ensure this happens constantly through the project lifecycle, and isn't a burden at the end
#     
# <small>Some taken from https://papers.ssrn.com/sol3/papers.cfm?abstract_id=1550193</small>


# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Common reasons to share code
#
# * Encouraging scientific advancement
# * Encouraging sharing and having others share with you 
# * Being a good community member
# * Increase in publicity
# * Improvement in the caliber of research
#
# <small>From https://papers.ssrn.com/sol3/papers.cfm?abstract_id=1550193</small>

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## How can we make reproducible research happen?

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Use git - version control

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Peer review code

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Automate everything: code as much of your process possible & remove manual repeated steps (an excellent source of untracable bugs)

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Use a consistent coding style (static checker) - a lack of readability is again a large source of bugs

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Use an issue tracker

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Document everything - e.g. jupyter notebooks, R markdown

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Use a written process - then bugs in the process can be improved upon

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# **=> implies a culture change is required**

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## In summary
# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Open reproducible research plays an important part in fixing the replication crisis
# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * You'll never write good code by yourself, as bugs being put into code is an invariant and taking them out involves collaboration
# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * It's the software engineering process that creates good software, not the people 
# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * If software development is the art of putting bugs into code, software engineering is the art of taking bugs out
# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * The best software engineering processes are collaborative
# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * A culture change is needed, the current way of working is not good enough
#     * Look out for the announcement of a DPMCN open reproducible research working group soon, please consider joining in!
# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # # Questions?
#
# <h3><center>Email: einonm@cardiff.ac.uk</center></h3>
# <h3><center>Chat: https://laggard.psycm.cf.ac.uk</center></h3>
# <center><img src="./headshot-2-crop.jpg" style="float: centre; width: 200px"></center>
