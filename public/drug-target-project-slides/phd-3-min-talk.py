# -*- coding: utf-8 -*-
# ---
# jupyter:
#   celltoolbar: Slideshow
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.3
#   kernelspec:
#     display_name: Bash
#     language: bash
#     name: bash
#   language_info:
#     codemirror_mode: shell
#     file_extension: .sh
#     mimetype: text/x-sh
#     name: bash
#   latex_envs:
#     LaTeX_envs_menu_present: true
#     autoclose: false
#     autocomplete: true
#     bibliofile: biblio.bib
#     cite_by: apalike
#     current_citInitial: 1
#     eqLabelWithNumbers: true
#     eqNumInitial: 1
#     hotkeys:
#       equation: Ctrl-E
#       itemize: Ctrl-I
#     labels_anchors: false
#     latex_user_defs: false
#     report_style_numbering: false
#     user_envs_cfg: false
# ---

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# <style>
# table,td,tr,th {border:none!important}
# </style>
#
# <h2><center>A genomic medicine approach to identifying novel drug targets</center></h2>
# <h3><center>Mark Einon</center></h3>
# <h3><center>Division of Psychological Medicine and Clinical Neurosciences 
# <br><br> Cardiff University</center></h3>
#
# <h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Supervisors:</h3>
#
# <table>
# <tr>
# <td>Prof. Nigel Williams</td>
# <td>Prof. Lesley Jones</td>
# </tr>
# <tr>
# <td>Prof. Peter Holmans</td>
# <td>Prof. Michael Owen</td>
# </table>
#
# <h4><center>Email: einonm@cardiff.ac.uk</center></h4>
# <h4><center><a href="http://einon.net/drug-target-project-slides">einon.net/drug-target-project-slides</a></center></h4>

# + {"slideshow": {"slide_type": "-"}, "active": ""}
# <script>
#   $( document ).ready(function(){
#     $('div.input').hide()
#     $('div.prompt').hide()
#   });
# </script>
#
#

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# #### Background
# * Studies¹ have demonstrated that for genes which are genetically associated with disease, the proteins for which they encode are likely to be enriched for existing targets of medications
# * These proteins are clear targets for drug development studies and offer opportunities re-purposing of existing approved drugs²
# * It has also recently been demonstrated³ that drugs supported by genetic associations are more likely to successfully advance through the later stages of clinical trials
#
# #### Hypothesis:
#
# * Identifying drugs that target proteins encoded by genes that increase risk to neuropsychiatric and neurological diseases will reveal novel therapeutic opportunities that can be used to inform and improve their treatment
#
# #### Project  Aims:
#
# * To develop a novel approach to investigate the intersection between disease risk loci and a curated database of known gene targets of all available therapeutic agents
#
# <br>
# <small><cite>1. Ruderfer DM, Charney AW, Readhead B, et al. Polygenic overlap between schizophrenia risk and antipsychotic response: A genomic medicine approach. The Lancet Psychiatry 2016;3(4):350–357
# <br>2. Navarese EP, Kolodziejczak M, Schulze V, et al. Effects of Proprotein Convertase Subtilisin/Kexin Type 9 Antibodies in Adults With Hypercholesterolemia: A Systematic Review and Meta-analysis. Ann. Intern.
# Med. 2015;163(1):40–51
# <br>3. Nelson MR, Tipney H, Painter JL, et al. The support of human genetic evidence for approved drug
# indications. Nat. Genet. 2015;47(8):856–860.</cite></small>

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ### Experimental approach
# Analyses will be entirely *in-silico*, using python, jupyter notebooks, git for version control and software tools that conveniently package current best practice statistical analyses (e.g. MAGMA)
#
# ##### Generate a drug targets/ontology database & curate a genomic map of functional genetic variants
#
# * Using available drug databases (DrugBank, SEA, PharmGKB) and the WHO's Anatomical Therapeutic Chemical Classification System (ATC) ontology.
# * In order to link risk variants (from a GWAS) to genes, Quantative Trait Loci (QTLs) are used - expression, splicing, methylation, exonic.
#
# ##### Identify pharmacologically related sets of drug targets that are associated with disease
#
# * Risk variant/disease association p-values used to generate gene/disease association p-values, and this gene set analysed against the drug target gene set. Output potentially a set of significant drug group/disease assocations.
#
# ##### Using proximity analysis to identify drugs with therapeutic potential
#
# * Uses these significant associations to identify contributing protein-protein interactions, and scoring the associations based on pathology of the disease.

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
#
# #### Potential impact of research
#
# * Identify existing drugs that could be potentially cross-purposed to treat a new disease, or even identify existing drugs that may adversly affect the onset of a disease.
#
# * Create a general analytical pipeline to enable further analysis of any disease / drug interaction set using other datasets.
#
# <br><br>
# <br><br>
#
# <small>Acknowledgements:</small><br>
# <img align='left' src="images/mrc-logo.jpg">
