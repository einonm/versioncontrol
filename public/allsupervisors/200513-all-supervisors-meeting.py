# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     cell_metadata_json: true
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.3
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + {"slideshow": {"slide_type": "skip"}}
import os
import sys
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import re
from IPython.display import display, HTML
from scipy import stats
import datetime as dt

# %matplotlib inline
import matplotlib.pyplot as plt

import qvalue

# + {"slideshow": {"slide_type": "skip"}}
pd.set_option('display.max_colwidth', -1)

data_path = '/home/mpnme/source/genomic-novel-targets/data/'
magma_results_dir = data_path + 'magma_analysis/magma_results/'
summary_results_dir = data_path + 'summaries/'

gwas = 'AD'
qtls = 'brain'
run_id = 'split_atc'


# + {"slideshow": {"slide_type": "skip"}}
## Summary table functions
# Returns ATC data (ATC code & description) in a dataframe, given an ATC table file of the same
def get_atc_file(filename):
    result = pd.DataFrame()
    with open(filename, 'r') as file: 
        lines = file.readlines()
        for line in lines:
            code = line.split(' ')[0]
            desc = ' '.join(line.split(' ')[1:]).strip()
            result = result.append({'atc_code':code, 'Description':desc}, ignore_index=True)
        
    return result

# Returns ATC data from all 5 ATC levels in one dataframe
def get_atc_levels():
    atc_level_files = [data_path + 'atc/level-' + str(i) + '-atc.txt' for i in range(1,6)]

    # convert each atc_level_file to a 2-column data frame, and cat all of them together 
    atc_levels = pd.concat([get_atc_file(atc_level_file) for atc_level_file in atc_level_files])
    atc_levels.set_index('atc_code', inplace=True)
    return atc_levels

# Read all results from a magma geneset analysis for this gwas and annotation type
def read_results_files(gwas, annot_type):
    prefix = magma_results_dir + gwas + '/results/' + 'magma_geneset_result-' + gwas + '-' + annot_type + '-' + run_id
    file = prefix + '-atc5.sets.out'
    
    return pd.read_csv(file, comment='#', delim_whitespace=True)

# Summarise results by applying significance and number of gene thresholds, and store in a file
# Generates results for both P and Q values.
def summarise_drug_results(gwas, annot_type, atc_levels, n_gene_thresh=5, signif_thresh=0.05):
    result = read_results_files(gwas, annot_type)
               
    # only consider classes/drugs with Qval < QVAL_THRESH and NGENES >= N_GENE_THRESH
    significants = result[result['NGENES'] >= n_gene_thresh]

    if significants.empty:
        print("returning - no significants for " + gwas + " " + annot_type)
        return

    # Calculate the q-values for the local results
    significants.loc[:, 'Q'] = qvalue.estimate(np.array(significants['P']))
    
    q_significant = significants[significants['Q'] < signif_thresh]
    q_final = pd.merge(q_significant, atc_levels, right_index=True, left_on='SET').sort_values('Q')
    q_final.to_csv(os.path.join(summary_results_dir, 'drugs_found-' + gwas + '-' +
                                run_id + '-' + annot_type + '_qvals.tsv'), sep='\t', index=False)

    p_significant = significants[significants['P'] < signif_thresh]
    p_final = pd.merge(p_significant, atc_levels, right_index=True, left_on='SET').sort_values('Q')
    p_final.to_csv(os.path.join(summary_results_dir, 'drugs_found-' + gwas + '-' +
                                run_id + '-' + annot_type + '_pvals.tsv'), sep='\t', index=False)

def summarise_gopath_results(gwas, annot_type, n_gene_thresh=5, signif_thresh=0.05):
    prefix = magma_results_dir + gwas + '/results/'
    file = prefix + 'magma_geneset_result-' + gwas + '-' + annot_type + '-' + run_id + '-gopaths.sets.out'
    
    if not os.path.exists(file):
        display("File not found: " + file)
        return
    
    result = pd.read_csv(file, comment='#', delim_whitespace=True)
               
    # only consider classes/drugs with Qval < QVAL_THRESH and NGENES >= N_GENE_THRESH
    significants = result[result['NGENES'] >= n_gene_thresh]
    
    if significants.empty:
        return
    
    significants.rename(columns={'SET':'SHORT_NAME','FULL_NAME':'SET'}, inplace=True)
        
    # Calculate the q-values for the local results 
    significants.loc[:, 'Q'] = qvalue.estimate(np.array(result['P']))
    
    q_significant = significants[significants['Q'] < signif_thresh]
    q_significant.to_csv(os.path.join(summary_results_dir, 'pathways_found-' + gwas + '-' + 
                                      run_id + '-' + annot_type + '_qvals.tsv'), sep='\t', index=False)

    p_significant = significants[significants['P'] < signif_thresh]
    p_significant.to_csv(os.path.join(summary_results_dir, 'pathways_found-' + gwas + '-' + 
                                      run_id + '-' + annot_type + '_pvals.tsv'), sep='\t', index=False)   

def __main__():
    annot_type_list = ['func-' + qtls, 'prox', 'hybrid-' + qtls]
    N_GENE_THRESH = 2
    SIGNIF_THRESH = 0.05
    ATC_LEVELS = get_atc_levels()
    for annot_type in annot_type_list:
        summarise_drug_results(gwas, annot_type, ATC_LEVELS, N_GENE_THRESH, SIGNIF_THRESH)
        summarise_gopath_results(gwas, annot_type, N_GENE_THRESH, SIGNIF_THRESH)
if __name__ == "__main__":
    __main__()
    
def get_annot_results(annot):
    file = os.path.join(magma_results_dir, gwas, 'results',
                        "magma_gene_result-" + gwas + "-" + annot + '-' + run_id + ".genes.out")    
    df = None

    if os.path.exists(file):
        df = pd.read_csv(file, delim_whitespace=True)
    else:
        display("File not found: " + file)
              
    return df

# All ATC genesets
atc_genesets_df = pd.read_csv(data_path + "drugbank/dbank_gene_set-all-atc.txt", 
                                 header=None, sep='\t', index_col=0)
# All GO genesets
go_genesets_df = pd.read_csv(data_path + '/magma_analysis/GO_ALL_PC_20-2000_MAGMA.LH.ensembl.druggable.txt',
                    header=None, sep='\t', index_col=0)
# All GO + ATC genesets
genesets_df = pd.concat([atc_genesets_df, go_genesets_df])
# Emsembl to HGNC translation table
gene_hgnc_df = pd.read_csv(data_path + '/wrangling/NCBI37.3.ensembl.gene.loc', sep='\t', header=None,
                           names=['Ensembl','1','chrom','chromStart','chromEnd', 'HGNC'])[['Ensembl', 'HGNC']]
# Hybrid (func + prox) gene analysis results
hybriddf = get_annot_results('hybrid-' + qtls)
# Functional gene analysis results
funcdf   = get_annot_results('func-' + qtls)
# Proximity gene analysis results
proxdf   = get_annot_results('prox')

def add_genesets(table, zscores_df):
    table['Ensembl'] = ""
    table['HGNC'] = ""
    table['ZSTAT'] = ""
    for index, row in table.iterrows(): 
        aset = genesets_df.loc[index].str.split(' ').tolist()
        setgenes_df = pd.DataFrame(aset).melt()[['value']]
        setgenes_df.columns = ['Ensembl']
        if not setgenes_df.empty:
            setgenes2_df = pd.merge(setgenes_df, zscores_df, how='left', 
                                    left_on='Ensembl', right_on='GENE')[['Ensembl', 'ZSTAT']]
            hugo_df = pd.merge(setgenes2_df, gene_hgnc_df, how='left', on='Ensembl')
            
            hugo_df['absZSTAT'] = hugo_df['ZSTAT'].abs()
            hugo_df.sort_values(by=['absZSTAT'], ascending=False, inplace=True)
            hugo_df.drop('absZSTAT', axis=1, inplace=True)
            hugo_df['ZSTAT'] = hugo_df['ZSTAT'].round(8).astype(str)
            # Limit to top 100 for now
            hugo_df = hugo_df.head(50)
            
            table.set_value(index, 'Ensembl', '\n'.join(hugo_df['Ensembl'].tolist()))
            table.set_value(index, 'HGNC', '\n'.join(hugo_df['HGNC'].fillna('-?-').tolist()))
            table.set_value(index, 'ZSTAT', '\n'.join(hugo_df['ZSTAT'].tolist()))
            
    # Hack for gosets
    if 'SHORT_NAME' in table.columns:
        table.set_index('SHORT_NAME', inplace=True)
        table.index.rename('SET', inplace=True)
            
# Display a set of results tables for the given GWAS list, file type and column (P or Q)
def display_tables(gwas, file_postfix, column, zscores_df):
    summary_dir = os.path.join(summary_results_dir)
    pval_files = [file for file in os.listdir(summary_dir) if file.endswith(gwas + '-' + file_postfix)]
    pval_files.sort()

    for file in pval_files:
        table = pd.read_csv(os.path.join(summary_dir, file), sep='\t')
        table.sort_values(column, inplace=True)
        table.set_index('SET', inplace=True)
        print('\033[1m\033[4m' + file[:-4] + '\033[0m - ' + str(len(table)) + ' items')
        if not table.empty:
            print('\033[1m\033[4m' + 'First 100 without geneset members displayed' + '\033[0m')
            if 'SHORT_NAME' in table.columns:
                display(table.drop('SHORT_NAME', axis=1).head(100))
            else:
                display(table.head(100))
           # add_genesets(table, zscores_df)
           # print('\033[1m\033[4m' + 'First 10 with first 50 geneset members displayed' + '\033[0m')
            #display(HTML(table.head(10).to_html().replace("\\n","<br>")))            


# + [markdown] {"slideshow": {"slide_type": "slide"}}
# <h1><center>A Genomic Medicine Approach to Identifying Novel Drug Targets<center></h1>
# <h1><center>Project update 13/05/2020</center></h1>
# <h3><center>Mark Einon</center></h3>
# <h3><center>MRC Centre for Neuropsychiatric Genetics and Genomics</center></h3>
# <h3><center>einonm@cardiff.ac.uk</center></h3>

# + {"slideshow": {"slide_type": "-"}, "active": ""}
# <script>
#   $( document ).ready(function(){
#     <!--$('div.input').hide()-->
#     $('div.prompt').hide()
#   });
#
# </script>

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## From the updated thesis outline: ##
#
# * **Chapter 2: An analysis pipeline to identify related sets of drug targets associated with disease**
#
#     * An assessment of the drug target databases that are available (uniqueness and overlap) leading to an objective decision on which should be included in order to capture most information
#     
#     * Design and construction of the database of drug targets, linking this to genes, and the analytical pipeline to interrogate GWAS
#     
#     * Testing the analysis pipeline using only the positional approach (as it is conventional) using diseases for which we have a known pathology and known drugs that work (call these Known Diseases). 
#

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ### Drug target definition ###
#
# For this study, a valid drug target is defined as a protein encoded by a human gene having an experimentally measured, associated & direct interaction with the drug.

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ### Drug target database curation ###
#
# Process consists of:
#
# * Literature search for drug-gene interaction databases (DGI DBs)
# * Manual curation to identify publications directly describing DGI DBs
# * Quality control to remove databases not suitable for this study (e.g. data not freely available)
# * Examination of each DB's features, comparing them against criteria for inclusion in this study's DGI DB and removal if not matching
# * Investigation of any relationships between remaining databases (such as one DB incorporating another), selecting smallest number that permits the largest coverage, to be merged together resulting in a final dataset suitable for this project.

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# #### Literature search, manual curation and QC ####
#
# * PubMed search for the terms 'drug', 'gene', 'interaction' and 'database'
# * Result: 646 publications which after manual curation reduced to 24 papers directly describing drug-gene interaction databases
# * These papers describe 18 DGI DBs in total, after removing those with unavailable data/websites
# * Searching for further 'update' publications on these 18 and other contributing DGI DBs gives a total of 74 publications - e.g. STITCH has 5 papers between 2008 and 2016 describing 5 DB versions
# * Result: 74 papers describing 18 databases going through to the next stage.

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# #### QC check of databases ####
#
# * The DGI DBs, via their publications and websites were checked against the following criteria:
#     * Approved drugs - Does the database contain substances that are approved drugs (having an ATC code)?
#     * Direct interactions - Ensure the drug and gene interaction does not happen through an intermediary, for example by both partaking indirectly in the same pathway.
#     * Not predicted - The interactions should not be predicted.
#     * Expert curated - Are the interactions manually curated by experts, or scraped directly by an algorithm?
#     * Based on experimental evidence - Are the interactions validated directly by experimental evidence?
#     * Human targets - Are the targets human proteins?

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# #### QC results ####
#
# | DGI DB Name | Abbreviation | Approved drugs | Interactions are direct? | Predicted | Expert curated | Experimental evidence | Human protein targets |
#  | --- | --- | --- | --- | --- | --- | ---| --- | 
#  | Therapeutics Target Database | TTD | Filter | Yes | No | Yes  | Yes | Yes | 
#  | Comparative Toxico-genomic Database | CTD  | Filter | Yes  | Filter | Yes | Filter | Filter | 
#  |<span style="color:red"> PubChem  </span>| PubChem | Filter| All data from DGIdb | - | -  | - | - | 
#  | DrugBank | DB | All | Yes | No | Yes | Yes | Yes |
#  | Search Tool for InteracTions of CHemicals | STITCH | Filter | Filter | Filter | Filter | Filter | Yes | 
#  | Target-toxin Database | T3DB | Filter | Yes | No | Yes | Filter | Yes |
#  |<span style="color:red"> SuperTarget </span>| ST  | Filter | Filter | Yes | Filter | Filter | Yes | 
#  | <span style="color:red">Manually Annotated Targets And Drugs Online Resource (broken link) </span>  | MATADOR | Filter | Filter | Filter | Filter | Filter | Yes | 
#  | <span style="color:red"> PharmacoGenetics Data, PharmGKB </span>| PGKB | Filter | Data from DrugBank and Pubchem (DGIdb) | - | - |- | -| 
#  | ChEMBL | ChEMBL | Filter | Yes | No | Yes | Yes | Yes |
#  | Drug Gene Interaction Database | DGIdb | Filter | Yes | No | Check contributors | Check contributors | Yes | 
#  | IUPHAR/BPS Guide to PHARMACOLOGY | GtoPdb | Filter | Yes | No | Yes | Filter | Filter | 
#  | International Union of Basic and Clinical Pharmacology db| IUPHARdb | Filter | Yes | No | Yes | Filter | Filter | 
#  | Drug SIGnatures Database | DSigDB | Filter | Yes | Filter | Yes | Yes | Yes | 
#  | Binding Database | BindingDB | Yes | Yes | No | Yes | Yes | Yes |
#  | Psychoactive Drug Screening Program | PDSP(Ki) | Filter | Yes | No | Yes | Yes | Yes |
#  | <span style="color:red">Connectivity Map </span>| CMAP | Yes | No | No | Yes | N/A | Yes |
#  | <span style="color:red">SuperLigands</span> | SL | Yes | No | Yes | N/A | No | Yes | 

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# #### Drug target inclusion criteria for each DB ####
#
# | DGI DB Name | Abbreviation | Inclusion criteria |
#  | --- | --- | --- | 
#  | Therapeutics Target Database | TTD | Note: Doesn't link a drug to the target-"The criteria for identifying the primary target of a drug or targets of a multi-target drug is based on the developer or literature reported cell-based or in vivo evidence that links the target to the therapeutic effect of the drug" (Zhu, F. et al. 2010. Update of TTD: Therapeutic Target Database. Nucleic Acids Research 38(Database issue), pp. D787–D791. doi: 10.1093/nar/gkp1014) | 
#  | Comparative Toxico-genomic Database | CTD  | "CTD curators developed a hierarchical vocabulary of 50 diverse terms (e.g. binding, phosphorylation, activity) to describe specific molecular interactions between a chemical and gene. A complete list of action codes with their definitions is available via the Help link for interactions on the CTD glossary page or query pages." (Davis, A.P. et al. 2009. Comparative Toxicogenomics Database: a knowledgebase and discovery tool for chemical-gene-disease networks. Nucleic Acids Research 37(Database issue), pp. D786-792. doi: 10.1093/nar/gkn580)|
#  | <span style="color:red">PubChem </span>| PubChem | Uses DGIdb as a source of drug-gene interaction data |
#  | DrugBank | DB | 'Mechanism of action' listed in DB - but many 'unknowns' "Drug targets are identified and confirmed using multiple sources (PubMed, TTD, FDA labels, RxList, PharmGKB, textbooks)" (Wishart, D.S. et al. 2006. DrugBank: a comprehensive resource for in silico drug discovery and exploration. Nucleic Acids Research 34(Database issue), pp. D668-672. doi: 10.1093/nar/gkj067); "565 non-redundant protein/DNA targets being identified for FDA-approved drugs compared to 524 non-redundant targets  dentified in release 1.0. The identification of so many more targets was aided by PolySearch... a text-mining tool developed in our laboratory to facilitate these kinds of searches" (Wishart, D.S. et al. 2008. DrugBank: a knowledgebase for drugs, drug actions and drug targets. Nucleic Acids Research 36(Database issue), pp. D901-906. doi: 10.1093/nar/gkm958)|
#  | Search Tool for InteracTions of CHemicals | STITCH | "...chemical–protein associations are integrated from pathway and experimental databases, as well as from the literature. Lastly, as many associations as possible are annotated with interaction types", "Experimental evidence of direct chemical–protein binding is derived from the PDSP K i Database (11) and the protein data bank (PDB) (23). Additional interactions between metabolites and proteins are extracted from the manually annotated pathway databases such as KEGG (12), Reactome (14) and the NCI-Nature Pathway Interaction Database (http://pid.nci.nih.gov), and drug–target relations are imported from DrugBank (8) and MATADOR (10)" (Kuhn, M. et al. 2008. STITCH: interaction networks of chemicals and proteins. Nucleic Acids Research 36(Database issue), pp. D684-688. doi: 10.1093/nar/gkm795) |
#  | Target-toxin Database | T3DB | "Target - Substances that the toxin chemically binds to" (http://www.t3db.ca/help/fields) |
#  | <span style="color:red">SuperTarget</span> | ST  | "We consider a drug-target relation as a specific interaction of a small chemical compound administered to treat or diagnose a disease and a macromolecule, namely protein, DNA or RNA." (Günther, S. et al. 2008. SuperTarget and Matador: resources for exploring drug-target relationships. Nucleic Acids Research 36(Database issue), pp. D919-922. doi: 10.1093/nar/gkm862); "Drug-target relationships described in SuperTarget were obtained in three different ways. Starting with 2400 drugs and their synonyms from the SuperDrug Database (5), the text mining tool EbiMed (6) was used to extract relevant text passages containing potential drug-target relations from about 15 millions public abstracts listed in PubMed. Many thousands of false positive or irrelevant relations were eliminated by manual curation. In parallel, potential drug-target relations were automatically extracted from Medline by searching for synonyms of drugs, proteins and Medical Subject Headings (MeSH terms) describing groups of proteins (7). MeSH terms were used to capture and down-weight interactions that are not explicitly described in the abstracts e.g. for protein families or protein complexes. In the case of families, the specific interacting family member might not be known yet, whereas in the case of complexes, the drug might interact with more than one subunit. Proteins associated to MeSH terms were assigned by a semi-automated procedure relying on mappings provided by MeSH and synonyms of proteins that are aggregated in the STRING resource (8). Proteins that were often mentioned in abstracts, but could not be automatically assigned to families, were manually assigned. Depending on the size and nature of the families, the confidence of an interaction between drugs and individual proteins was decreased. More heterogeneous families are assigned a lower confidence. The most probable candidates were identified using a benchmarking scheme (8) and manually curated. In a last step, relations from other databases, namely DrugBank (3), KEGG (9), PDB (10), SuperLigands (11) and TTD (4), were checked for drug-target interactions not identified with the preceding steps. If those interactions could be confirmed by literature listed in PubMed, the references were included in SuperTarget otherwise the describing database is referenced." (Hecker, N. et al. 2012. SuperTarget goes quantitative: update on drug–target interactions. Nucleic Acids Research 40(Database issue), pp. D1113–D1117. doi: 10.1093/nar/gkr912)|
#  | <span style="color:red">Manually Annotated Targets And Drugs Online Resource </span> | MATADOR  | "For a subset of the drug-target relations, namely those where our text-mining approach indicated a wealth of additional information, the type of binding was further analyzed and direct and indirect interactions were manually distinguished. Indirect interactions can, for example, be caused by active metabolites of the drug or by changes in the expression of a protein." (Hecker, N. et al. 2012. SuperTarget goes quantitative: update on drug–target interactions. Nucleic Acids Research 40(Database issue), pp. D1113–D1117. doi: 10.1093/nar/gkr912) |
#  | <span style="color:red">PharmacoGenetics Data, PharmGKB</span> | PGKB | "Drug information including pharmacological effects, mechanisms of action, and structures was obtained from Drugbank [4] and Pubchem [9]." (Thorn, C.F. et al. 2013. PharmGKB: the Pharmacogenomics Knowledge Base. Methods in Molecular Biology (Clifton, N.J.) 1015, pp. 311–320. doi: 10.1007/978-1-62703-435-7_20) |
#  | ChEMBL | ChEMBL | "..targets (the proteins or systems being monitored by an assay)" (Gaulton, A. et al. 2012. ChEMBL: a large-scale bioactivity database for drug discovery. Nucleic Acids Research 40(Database issue), pp. D1100–D1107. doi: 10.1093/nar/gkr777) | 
#  | Drug Gene Interaction Database | DGIdb | "Interactions in DGIdb are defined as a relationship between a gene and a drug with an associated interaction type (e.g., inhibitor) from a specified source." (Griffith, M. et al. 2013. DGIdb: mining the druggable genome. Nature Methods 10(12), pp. 1209–1210. doi: 10.1038/nmeth.2689) |
#  | IUPHAR/BPS Guide to PHARMACOLOGY | GtoPdb | "Our generic use of the term ‘target’ refers to a record in the database that has been resolved to a UniProtKB/Swiss-Prot ID as our primary identifier." (Southan, C. et al. 2016. The IUPHAR/BPS Guide to PHARMACOLOGY in 2016: towards curated quantitative interactions between 1300 protein targets and 6000 ligands. Nucleic Acids Research 44(D1), pp. D1054-1068. doi: 10.1093/nar/gkv1037) |
#  | International Union of Basic and Clinical Pharmacology db| IUPHARdb | (An older version of GtoPdb) | 
#  | Drug SIGnatures Database | DSigDB | Collects targets in 4 collections, based on different criteria: "D1: approved drugs...we obtained all the approved drugs from US FDA website, and retrieved bioactivity data for these drugs from PubChem and ChEMBL. Genes with ‘active’ bioassay results recorded in these databases were compiled as the drug target genes" Others do not match(?) D2: kinase inhibitors, D3: perturbagen signatures, D4: computational drug signatures. (Yoo, M. et al. 2015. DSigDB: drug signatures database for gene set analysis: Fig. 1. Bioinformatics 31(18), pp. 3069–3071. doi: 10.1093/bioinformatics/btv313) | 
#  | Binding Database | BindingDB | "..database of measured protein–ligand affinity data" - implied that a target is a protein binding to a drug with affinity measurement evidence (Gilson, M.K. et al. 2016. BindingDB in 2015: A public database for medicinal chemistry, computational chemistry and systems pharmacology. Nucleic Acids Research 44(D1), pp. D1045–D1053. doi: 10.1093/nar/gkv1072) | 
#  | Psychoactive Drug Screening Program | PDSP(Ki) | "...provides information on the abilities of drugs to interact with an expanding number of molecular targets. The Ki database serves as a data warehouse for published and internally-derived Ki, or affinity, values for a large number of drugs and drug candidates" (https://pdsp.unc.edu/databases/kidb.php) | 
#  | <span style="color:red">Connectivity Map</span> | CMAP | No binding targets - gene expression signatures only.| 
#  | <span style="color:red">SuperLigands </span>| SL | No binding targets - predicted by tanimoto similarity only.  | 

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ##### Cross-inclusion of databases #####
#
# This is where the other DB contents are imported directly into the DB.
#
# | DGI DB Name | Abbreviation | Included DBs |
#  | --- | --- | --- | 
#  | Therapeutics Target Database | TTD | |
#  | Comparative Toxico-genomic Database | CTD | |
#  | DrugBank | DB | (x-checked only with PharmGKB and TTD) |
#  | Search Tool for InteracTions of CHemicals | STITCH | CTD, DB, MATADOR, PDSP, PDB, BinidingDB, PGKB, GLIDA |
#  | Target-toxin Database | T3DB | (x-checked only with CTD and STITCH) |
#  | ChEMBL | ChEMBL | |
#  | Drug Gene Interaction Database | DGIdb | PGKB, DB, TTD, TEND, TALC, MyCancerGenome |
#  | IUPHAR/BPS Guide to PHARMACOLOGY | GtoPdb | |
#  | Drug SIGnatures Database | DSigDB | |
#  | Binding Database | BindingDB | (D1 and D2 only) |
#  | Psychoactive Drug Screening Program | PDSP(Ki) | | 

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# #### The relationships between DGI databases, their primary sources and each other resulting from the literature search is captured in the network diagram below: ####
#
# <img src="./graph.svg">

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# From this information, a set of DGI databases were selected to provide maximum coverage of the graph, taking inclusion of other DGI databases into account. The drug-gene interaction sources chosen were: STITCH, TTD, DrugBank, DGIdb, GtoPdb and DsigDB. (TODO - also look at DrugCentral?)

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# ## Points from March meeting ##
#
# * Presented comments from the November annual progress review                                                                                                                                                         
# * Update thesis plan and share - Done
# * Be specific / describe that drug-gene interactions are actually drug-protein interactions which are described using the gene that produces the protein. 
# * Use diabetes(II) to compare the overlap between genesets in pathways and drug targets - Done, added to set of 'Known Diseases'
#
#
# * TODO - For each well-understood disease, compile a list of approved drugs used to treat
#     * WIP - Using ATC classicfications, will look at NICE guidelines
# * TODO - Similarly, present the differences (in power) between hybrid/positional/functional
#     * WIP - Looking at calculating power for gene set analysis
#
#
# * TODO - using the 363 drugs common to all 5 DGI databases, score the quality of each database  
# * TODO - Consider using MAGMA with covariates to see the effects of inputs from different databases.
# * TODO - Compile simpler pathways vs drugs geneset overlap analysis result and share 
# * TODO - present the differences in resultant SNP gene annotations between mapping genes functionally and positionally.                                                                                            
#  
#
#

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# * **Chapter 2: An analysis pipeline to identify related sets of drug targets associated with disease**
#
#     * An assessment of the drug target databases that are available (uniqueness and overlap) leading to an objective decision on which should be included in order to capture most information
#     
#     * Design and construction of the database of drug targets, linking this to genes, and the analytical pipeline to interrogate GWAS.
#     
#     * Testing the analysis pipeline using only the positional approach (as it is conventional) using diseases for which we have a known pathology and known drugs that work (call these Known Diseases)  
#

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# * **Chapter 3: Using Functional gene annotation to better identify genes associated with disease** 
#
#     * An assessment of the drug target databases that are available (uniqueness and overlap) leading to an objective decision on which should be included in order to capture most information
#     * Design and construction of the database of drug targets, linking this to genes, and the analytical pipeline to interrogate GWAS.
#     * Testing the analysis pipeline using only the positional approach (as it is conventional) using diseases for which we have a known pathology and known drugs that work (call these Known Diseases)

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# * **Chapter 4: Using Biological Pathway and Network data for drug repurposing**
#     * Developing the Gene Ontology approach i.e. identifying drugs that interact with a biological Gene Ontology pathway that has been associated with a disease via GWAS (and possibly TWAS)
#     * Assess using Known Diseases and conventional statistical tests
#     * Consider more novel network analyses of the same data and compare to the results obtained using conventional statistical tests
#     * Apply the method to Unknown Diseases   

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# ## Progress Review Chapter (Submitted November) ##
#
# * Covered drug-gene interaction dataset curation, used as input into the gene set enrichment analysis
#
# <img src="./summary-analysis.png">

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# #### Drug-gene interaction dataset curation - DGI inclusions
#
#
# <img src="./dgidb_network.png">

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# ## Drug-gene interaction dataset curation
#
# * Literature search uncovered 18 drug-gene interaction databases with available data, 5 were chosen with most coverage and analysed, converted into a canonical form and merged.
#
# |DGI source|ATC drugs|ATC drugs >= 5 targets|Total targets|
# | --- | --- | --- | --- |
#  |DrugBank|1268|358|1610|
#  |STITCH  |1239|545|4804|
#  |DGIdb   |1192|509|1622|
#  |GtoPdb  |668|108|452|
#  |TTD     |924|4|279|
#  |Curated|1749|878|5460|
#  
#  

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# #### Drug-gene interaction database shared and individual drug contributions
#
# <img src="./venn-diagram.png">

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# #### Boxplot diagram of target gene coverage from each drug-gene interaction source
#
# <img src="./boxplot-dgi.png">

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# ## Review comments ##
#
#  * Regarding the pathways analysis plan (specifically the one based on a hybrid of functional annotation where the plan is to use positional where this is no functional annotation); the proposal may be valid, but Mark needs to be able to clearly describe why this is better than the alternative of a ‘pure’ positional analysis versus a ‘pure’ functional annotation analysis.   

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# ## Review comments (cont.) ##
#
#  * In terms of the planned structure of the thesis, Mark should consult further with the supervising team. Under the current plan, the submitted report would be the basis of a chapter. As it stands, while it clearly involves a lot of work, it seems a bit more like a methods chapter. It can probably be converted to a results chapter by careful consideration and assessment of some of the above issues. 
#
#  * We suggested a slight restructuring (but please consult with supervisors for definitive guidance) where the next chapter should be application to positional data, followed by a chapter of application to functional annotation.  This would then be followed by the network chapter. This latter chapter is yet to be fully thought through by Mark, but he was clear there is an accepted methodology so he at least anticipated no problems. 

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# ## Review comments (cont.) ##
#
# * Some challenges found in understanding the pros and cons of combining the data;
#      * should he use all targets that appear in any database, or some intersection of databases? 
#      * Are all databases of equal quality and if not, is there a way of allowing that?
#      * How to deal with drug metabolism enzymes that are ‘co-incidental’ targets?
#  

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# ## Overview - justification
#
# * Drug discovery is expensive with a high risk of failure - drug repurposing a less risky fast track to finding novel therapeutic interventions.
#
#
# * Drugs supported by genetic associations to disease more likely to be successful. Amount of available genetic data on drugs and their targets is increasing at a rapid pace - here, the drug targets are proteins produced from genes.
#
#
# * Gene set enrichment analysis, used to identify a small group of drug target genes over-represented within the set of genes associated with a disease which may indicate repurposing opportunities.
#
#
# * Additional evidence from network analysis - using protein-protein and drug-target interaction data examining the relationship with of clusters of genes associated with a disease (disease module).

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# ### Drug-gene interaction dataset curation
#
# #### Successes 
#
# * More drugs, more targets.
# * A drug target defined as a protein that binds to a drug, backed by experimental evidence.
#
# #### Issues
#
# * ATC drugs that are combinations of one or more compounds are not handled, resulting in ~150 drugs with >= 5 targets being dropped.
#
#
# * Use of PubChem compound IDs results in duplicate IDs for some compounds (more likely for approved drugs). InChI Keys are not susceptible to this.
#
#
# * Unifying definition of a drug target is difficult to achieve. Current set of evidence known to have some predicted evidence for DGIs, an analysis of the gathered evidence for DGIs of interest will be required to eliminate.

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# ## Points from November meeting
#
# * Big picture/overview presented
# * Drug-gene interaction discussed

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# ## Points from September meeting
#
# * Talk to pharmacologist / Takeda - what data formats used, what is the criteria for inclusion of DGI?
#     * MRC Takeda team have no direction from Takeda on this.
#     
#     
# * What is a drug-gene interaction? Define one to create a consistent dataset.
#     * Constrained by constituent DGI database definitions (if they exist) - collect and compare.
#     
#     
# * Come up with description of 'big picture'.

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# ## Points from May meeting
#
# * Running all ATC level sets together reduced the significance of results to nothing
#     * Selecting a sub-set of the levels (3 and 5) gave some significant results, but also the 3rd level sets broadly reflected the 5th level significant sets - is this useful?
#     * Results below are for 5th level ATC sets only.

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# ## Points from June meeting
#
# * Use just the N subset as a competitive gene set? 
#     * Using individual drug gene sets only, and for the near future
#                                                      
#                                                      
# * Sensitivity analysis - Remove top performing gene (CACNA1C?) and run again. 
#     * Done for SZ, see results later
#     * Case for gene set analysis of drug targets justified?
#                                                
#                                                
# * Look at drug-gene interaction evidence - is it believable?
#     * Done, see results later
#                                              
#                                              
# * Source PGC3 SZ dataset?
#     * Not yet. Concentrating on test data of well-understood disease GWAS.
#     
#     
# * Also ran a few different GWAS gwene sets against drugs annotated with a more comprehensive set of DGIs.
#     * Now using DGIdb, STITCH, DrugBank, GtoPdb, TTD databases.
#     * Number of annotated drugs shift up to 924 from 473

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# ## Points from July meeting
#
# * Run with experimentally obtained DGI data only -> No text mining                                                                                                                       
# * Check HMGCR gene / hypertension for missing z-value in statins -> why no z-val?
#     * Bug in dataset generation, now fixed.
#
#
# * WHY DO STATINS APPEAR HIGHLY CORRELATED WITH RELEVANT DISEASES IF TARGET GENE NOT FEATURING IN ANALYSIS?                                                                            
#     * unknown
#
#
# * Choose 1 example (Hypertension) and drill down into all reasons for associations -> remove CYP genes / Matador indirect, threshold evidence etc.
#     * Automation of this WIP 
#                         
#                         
# * Remove CYP / Add thresholds to DGI dbs 
#     * WIP 
#                   
#                   
# * Ask for PGC3 SZ/CLOZUK GWAS to run through pipeline (TODO)   
#
#
# * Run with ATC lvl3 (or whatever Breen group used)    
#
#
# * Run analysis on Bipolar GWAS

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# # Anatomical Therapeutic Chemical (ATC) classification system
#
# * Set up by the WHO to to serve as a tool for drug utilization monitoring and research in order to improve quality of drug use
# * A major aim ... is to maintain stable ATC codes and DDDs (Defined Daily Dose) over time to allow trends in drug consumption to be studied without the complication of frequent changes to the system
# * Classifies substances in a heirarchy with 5 levels
#
# |Code |Desc |
# |--|--|
# |A 	| Alimentary tract and metabolism (1st level, anatomical main group)|
# |A10 | Drugs used in diabetes (2nd level, therapeutic subgroup)|
# |A10B | Blood glucose lowering drugs, excl. insulins (3rd level, pharmacological subgroup)|
# |A10BA |	Biguanides (4th level, chemical subgroup)|
# |A10BA02 | metformin (5th level, chemical substance)|
#

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# ## ATC
#
# * Allows drug targets to be grouped according to chemical, pharmacological or therapeutic properties
# * Provides larger drug target groups that potentially have a shared relationship with a disease

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# * Most interesting results are with Hybrid prox/QTL GWAS gene annotation
#     * Difference between hybrid and prox only results also interesting
#     * with specialised QTL tissue types (ass opposed to all available)
#     * Gene sets for comparison are ATC level 5 only
#     * Well powered GWAS important

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# #  Results Chapters / Hypotheses
#
# 1. **Drug targets / disease gene database** - Generate sets of functionally mapped genes associated with a disease. Curate a DB of drugs and targets from existng data sources. This should provide data required for the subsequent gene set analysis. 
#
# 2. **Gene set analysis** - groups of genes that are targets for approved drugs and highly associated with gene sets which increase risk to disease may indicate repurposing opportunities.
#
# 3. **Protein-protein interaction (PPI) network analysis** - proteins that are targets for approved drugs that are located close to proteins associated with a disease in the protein-protein interactome may indicate further repurposing opportunities and also reveal potential mechanisms of action of the drugs in treating the disease.

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# # Drug Targets / Disease Gene Database
#
# ### Progress
# * 1st iteration of pipeline performed using Drug Target Interactions (DTIs) from DrugBank only
# * Literature search for potential data sources performed
# * Searching for "drug target" and "database" on pubmed gave ~500 results, distilled down to 24 papers after a manual check for matching criteria (approved drugs, measured interaction & not predicted, < 10yrs old, ...).
# * Network diagram created showing raw DTI sources and data flow between DTI databases
# * Resultant DB to be conceptually a table listing Genes (Entrez) / Contributing SNPs (rs) / Gene p-values / Drugs targetting (PubChem CID & ATC code), for genes that functionally map from GWAS SNPs and have at least one drug targetting it.
#
#
# ### Results
# * SNPs functionally mapped using GTEx + some mQTLs, any missing GWAS genes populated using positional data (hybrid approach).
# * Currently [TTD and DrugBank DBs](drug_target_crossover.html) scraped and converted to this format - large overlap between data held in both (DrugBank is the more comprehensive of the two).
#

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# ## Drug Target Interaction DBs
# ![title](graph.png)

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# # Gene Set Analysis
#
# ### Progress
# * Pipeline created and tested which analyses functionally mapped GWAS associated genes against drug targets alternately using brain specific tissue QTLS and all GTEx QTLs; also GO term gene sets run as a separate analysis.
# * Validation of method attempted using simpler, known diseases with approved drugs available to treat - hypertension and high cholesterol (from [GeneAtlas BioBank GWAS](http://geneatlas.roslin.ed.ac.uk/) results).
# * Prediction of candidate repurpose drugs attempted for PD.
#
# ### Results
# * For [high cholesterol](concise_results_summary_GA_E78-brain.html), Amlodipine is prevalent in a collection of significantly associated drugs & drug combinations (driven mainly by ACE & CACNA2D3 genes), with 3 significant GO terms, all related to lipids & choloesterol. (Using positional gene mapping, no drugs are significantly associated & anti-neoplastics top hit).
# * For [hypertension](concise_results_summary_GA_I10-brain.html), top 3 significant drugs for brain tissue QTL mapping are all anti-hypertensive agents (through ACE & CACNB2, amongst others). Notably the all tissue type QTL mapping fails to flag any significant associations.
# * For [Parkinson's disease](concise_results_summary_PD2-nb.html) a large number of immune system drugs are implicated as potential repurposing opportunities (e.g. Adalimumab, Natalizumab and Palivizumab), with the significant association mainly being driven by the TNF, FCGR3A and C1QA genes. 

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# # Protein-Protein Interaction Network Analysis
#
#
# ### Progress
# * Identified open source graph database software suitable for thisd analysis (Neo4j) and created scripts to enable ata ingress. 
# * Populated graph DB with PD GWAS gene set, DTI data and human interactome PPI network data (Menche et al, 2015) and GO interaction pathways.
# * Created several graph DB queries to perform cursory investigation of the data: Find common GO paths  between drug and gene of interest, Find within PPI links between genes of interest and drug target, Find GO paths that two genes of interest and drug target partake in...etc.
#
# ### Results
# * Simple queries regarding known disease genes of interest and significant drug associations found return plentiful results.
# * Indicates possibility of asking many complex questions (e.g. "which drugs having an association with a disease (Z score > 5), also have shared GO paths where a drug target gene and significant disease gene (given by p-val < 0.00001) have 2 or less interacting genes between them?").

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# # Example PPI query
# ### Find common GO pathways shared between drug Etanercept and gene GBA
# MATCH (a:ATC {code: 'L04AB01'}), p = (a)-[:member_of|is_in*3]-(:gene {HGNC: 'GBA'}) return p
# ![title](graph2.png)

# + [markdown] {"slideshow": {"slide_type": "skip"}}
# # Further work ideas
#
# * Gene set analysis limited to drugs with betweeen 10-200 target genes (limit of <5 used so far), what about drugs with a number of target genes not in this range?
#
#

# + {"slideshow": {"slide_type": "-"}}
from IPython.display import display, HTML
HTML('''<script>
code_show=true; 
function code_toggle() {
 if (code_show){
 $('div.input').hide();
 } else {
 $('div.input').show();
 }
 code_show = !code_show
} 
$( document ).ready(code_toggle);
</script>
''')
# -


