# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 1.0.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + {"slideshow": {"slide_type": "skip"}}
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import re
from IPython.display import display, HTML
from scipy import stats
import datetime as dt

# %matplotlib inline

BLOCK_SIZE = 1024
DATA_DIR = '~/source/sequencers_data_size/data/'

# + {"slideshow": {"slide_type": "skip"}}
dirs = [
        "hiseq4000bk",
        #"protonbk",
        #"protonbk2",
        #"protonpgmbk",
        #"proton-vnx",
        #"protonbk_archivedReports",
        #"protonpgmbk_archivedReports",
        #"proton-vnx_archivedReports",
       ]

appended_data = []
for dir in dirs:
    df = pd.read_csv(DATA_DIR + dir, sep='\t', header=None)
    df.columns = ["Blocks", "Dir"]
    df["Bytes"] = df["Blocks"] * BLOCK_SIZE
    df["TB"] = df["Bytes"] / (1024**4)
    df["Timestamp"] = ''

    if dir == "hiseq4000bk":
        # HiSeq data is double due to processing
        df["TB"] = df["TB"] * 2
        for index, row in df.iterrows():
            datetimestr = row["Dir"][12:18]
            try:
                year = int("20" + datetimestr[0:2])
                mon = int(datetimestr[2:4])
                day = int(datetimestr[4:6])
                timestamp = dt.datetime(year, mon, day)
            except:   
                timestamp = dt.datetime.now()
            df.set_value(index, 'Timestamp', timestamp)

    elif dir == "proton-vnx_archivedReports" or dir == "protonbk_archivedReports" or dir == "protonbk2_archivedReports":
        for index, row in df.iterrows():
            datetimestr = row["Dir"][len(dir)+3:len(dir)+14]
            try:
                year = int(datetimestr[0:4])
                mon = int(datetimestr[5:7])
                day = int(datetimestr[8:10])
                timestamp = dt.datetime(year, mon, day)
            except:   
                timestamp = dt.datetime.now()
            df.set_value(index, 'Timestamp', timestamp)    
    else:        
        for index, row in df.iterrows():
            datetimestr = row["Dir"][len(dir)+3:len(dir)+22]
         #print(datetimestr)
            try:
                divs = list(map(int, datetimestr.split("_")))
                timestamp = dt.datetime(divs[0], divs[1], divs[2])
                df.set_value(index, 'Timestamp', timestamp)
            except:
                timestamp = dt.datetime.now()
            df.set_value(index, 'Timestamp', timestamp)
                       
    appended_data.append(df)
               
df_tot = pd.concat(appended_data)
df_tot = df_tot.sort_values(by=['Timestamp'])
df_tot["Cumulative"] = df_tot['TB'].cumsum()
df_tot.reset_index(inplace=True)
df_tot['exomes'] = df_tot.index * 96

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# <h1><center>Gregynog 2019 - Data Management<center></h1>
# <h1><center>Data Management Practicalities</center></h1>
# <h3><center><a href="http://einon.net/datamgmt">einon.net/datamgmt</a></center></h3>
# <h3><center>Mark Einon, HPC Systems Manager</center></h3>
# <h3><center>MRC Centre for Neuropsychiatric Genetics and Genomics</center></h3>
# <h3><center>einonm@cardiff.ac.uk</center></h3>

# + {"active": ""}
# <script>
#   $( document ).ready(function(){
#     <!--$('div.input').hide()-->
#     $('div.prompt').hide()
#   });
#
# </script>

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# #  Operational setup
#
# Considering only replicated high-capacity archive storage
# * initially selected for RDIM pilot programme and WGP WG awarded storage
# * used for Neurocluster and DRI storage
#
# ### ARCCA collaboration on
#
# * Purchasing
# * Managing physical servers - installation, changing failed disks.
# * Provisioning / disaster recovery of OS - Shared scripts (using git), mainly authoured by DPMCN HPC team.
#

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ###  Support provided by DPMCN HPC team for NeuroCluster
#
# * User permissions and access managmement
# * Specialising storage volumes based on usage profile - Sequencers, Databank, Backups, Filesync, Projects, Biobank
# * Per-volume performance optimisation
# * Monitoring servers / setting up and managing alerts
# * Authoring quotes and DMPs for grants & fellowships
# * Experiment design / coding, scripting & licencing support
# * Managing supporting collaboration services - wiki, gitlab, public research sites
#

# + {"slideshow": {"slide_type": "skip"}, "cell_type": "markdown"}
# # DPMCN physical infrastructure
#
# * Currently 8 storage servers (3 pairs of storage servers + arbiter (corpuscallosum!) + monitoring server)
# * Free pool  of > 100TB kept for no delay in allocations
# * Data has university wide permissions (originals all converted, new data has applied) 
# * Collaborations servers - gitlab, wiki, public research data sites, mattermost, cobbler (provisioning)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# #  Neurocluster data management policy
#
# * Driven by funders motivating for open data, open science; and a practical desire to minimise compute use, storage and people time:
#     * create easiest path to publish data and code
#     * validation that the code and raw used creates published results
#     
# * This translates to using reproducible, shareable pipelines ensuring repeatable results
#     * Modern best practices support these aims, but not widely adopted
#     * We promote the use of git, Jupyter notebooks
#     * Practices should result in archiving only raw data and results (all else can be re-generated).
#     
# #### What about the cloud?
#
# * Not much chance currently, as we don't have eligible data? ...see http://einon.net/cloud-bigdata
#
# + {"slideshow": {"slide_type": "-"}}
from IPython.display import display, HTML
HTML('''<script>
code_show=true; 
function code_toggle() {
 if (code_show){
 $('div.input').hide();
 } else {
 $('div.input').show();
 }
 code_show = !code_show
} 
$( document ).ready(code_toggle);
</script>
''')

# + {"slideshow": {"slide_type": "skip"}, "cell_type": "markdown"}
# # linear regression
# df_tot['unix']= df_tot['Timestamp'].map(dt.datetime.toordinal)
# slope, intercept, r_value, p_value, std_err = stats.linregress(df_tot['unix'],
#                                                                df_tot['Cumulative'])
#
# plt.plot(df_tot["Timestamp"], df_tot["Cumulative"], marker='o')
# plt.xticks(rotation=25)
#
# ser = pd.Series([dt.datetime(2022, 1, 1), dt.datetime(2021, 1, 1)])
# ser_unix = ser.map(dt.datetime.toordinal)
#
# mn = np.min(df_tot['unix'])
# mx = np.max(ser_unix)
# x1 = np.linspace(mn, mx, 500)
# y1 = slope * x1 + intercept
# plt.plot(x1, y1, '-r')
# plt.title("Data usage and prediction for HiSeq exome data")
# plt.xlabel('Time')
# plt.ylabel("TB")
# plt.show()
#
# # result at 1st Jan 2022?
# print("Estimation of usage on Jan 1st 2022: " + str(int(slope * ser_unix[0] + intercept)) + "TB")

# + {"slideshow": {"slide_type": "skip"}, "cell_type": "markdown"}
# df_tot['unix']= df_tot['Timestamp'].map(dt.datetime.toordinal)
# slope, intercept, r_value, p_value, std_err = stats.linregress(df_tot['unix'],
#                                                                df_tot['exomes'])
# plt.plot(df_tot["Timestamp"], df_tot["exomes"], marker='o')
# plt.xticks(rotation=25)
#
#
# mn = np.min(df_tot['unix'])
# mx = np.max(ser_unix)
# x1 = np.linspace(mn, mx, 500)
# y1 = slope * x1 + intercept
# plt.plot(x1, y1, '-r')
# plt.title("Quantity of exomes stored prediction for HiSeq exomes")
# plt.xlabel('Time')
# plt.ylabel("Number of exomes")
# plt.show()
#
# # result at 1st Jan 2022
# print("Estimation of exomes stored on Jan 1st 2022: " + str(int(slope * ser_unix[0] + intercept)) + " exomes")
# -



