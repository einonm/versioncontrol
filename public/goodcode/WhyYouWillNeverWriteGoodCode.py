# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     cell_metadata_json: true
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# <h1><center>ARCCA HPC Research Seminar 12-02-20<center></h1> 
# <h1><center>Why we will never write good code</center></h1>
# <h3><center><a href="http://einon.net/goodcode">einon.net/goodcode</a></center></h3>
# <h3><center>Mark Einon, HPC Systems Manager</center></h3>
# <h3><center>MRC Centre for Neuropsychiatric Genetics &amp; Genomics</center></h3>
# <h3><center>einonm@cardiff.ac.uk</center></h3>

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Why we will never write good code
#
#
# * The code we author will always be full of bugs
#
# <img src="./cc_bug.jpg" style="float: left; width: 200px; height: 200px;">
# <img src="./cc_bug2.jpg" style="float: left; width: 200px; height: 200px;">
# <img src="./inquisition.webp" style="float: left; width: 260px; height: 200px;">
# <img src="./cc_bug4.jpg" style="float: left; width: 200px; height: 200px;">

# + {"slideshow": {"slide_type": "-"}, "active": ""}
# <script>
#   $( document ).ready(function(){
#     <!--$('div.input').hide()-->
#     $('div.prompt').hide()
#   });
#   
# </script>

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * A bug is unexpected or incorrect behaviour

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## Software bugs
#
# * Rear Admiral Grace Hopper is noted to have popularised the term, having made a note about a real bug causing issues in an early computer, the Harvard Mark II, in 1946.

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# <img src="./first_bug.jpg" style="display: block; margin-left: auto; margin-right: auto; width: 460px;">

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * (Although the term 'bug' referring to a technical glitch has been around at least since Thomas Edison's time / 1870s)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# #### A programmer is going to the grocery store, so their partner asks,
#
# ### "Buy a pint of milk, and if there are eggs, buy a dozen." 

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# So the programmer goes, buys everything, and drives back to the house.

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# Upon arrival, the partner angrily asks them,
#
# ##### "Why did you get 13 pints of milk?" 

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# The programmer says, 
#
# ## "There were eggs!"

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## Back to why you'll never write good code...

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# ### It's not you, it's Us 

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Humans are very good at making mistakes

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Developing code is a complex process, so humans make lots of mistakes!
#
# <img src="./bobross.jpg" style="display: block; margin-left: auto; margin-right: auto; width: 300px">

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Bugs being put into code is an invariant, we don't have a way of changing this and probably never will
#     * **Software development is the art of putting bugs into code**

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## But what about the professionals? I bet NASA doesn't release buggy code...

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Yes, it does! They're acutely aware of it and NASA attempts to monitor defect rates closely (defects = bugs)

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * NASA acknowledged a defect rate of ~ 1 in every 1000 lines of code for flight software in 2009 [1] 
#
# <img src="./nasa-report.png" style="display: block; margin-left: auto; margin-right: auto; width: 700px">
#
# <small> 1. https://www.nasa.gov/pdf/418878main_FSWC_Final_Report.pdf </small>

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## Catch-22
#
# * Take the **Lockheed Martin F-22 Raptor** for example:
#     * 2.5 million lines of code
#     * 80% of functionality controlled by software...
#
# <img src="./f22.jpg" style="display: block; margin-left: auto; margin-right: auto; width: 400px">

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * **Estimated to have ~2,500 bugs**

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## Shuttle bug
#
# * Space shuttle Primary Avionics Systems Software (PASS) had 420,000 lines of code, with only 1 bug present in the last version (0.002 defects/KLOC)
# <img src="./shuttle.jpg" style="display: block; margin-left: auto; margin-right: auto; width: 400px"> 

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * NASA spent ~20 times more per line of code than average to get this ($1/2 billion)
#

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * The shuttle software engineers removed **99.9% of bugs prior to use**
#     * **These bugs were still present in the code before they were removed!**

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## So where does that leave the rest of us?
#
# * Industry average is estimated to be **15-50 defects** per 1000 lines of code [1]

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Microsoft apps estimated to have **10-20 defects** per 1000 lines before QC, **0.5 defects/KLOC** on release (high quality commercial) [1]
#     * Using experienced, professional software engineers, following best practice
#     

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Linux OS measured to have **0.17 defects** per 1000 lines (best open source) [2]
#     * Mixture of hobbyist and professional software engineers
#     * Success relies heavily on the processes employed to develop the code, a.k.a. open source best practices
#     * Linus' Law: **'given enough eyeballs, all bugs are shallow'**
#
# <small>1. https://www.mayerdan.com/ruby/2012/11/11/bugs-per-line-of-code-ratio</small>
# <br><small>2. https://www.helpnetsecurity.com/2013/05/07/analyzing-450-million-lines-of-software-code/ </small>

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# ## It's the process that creates good software, more than the people 

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## So, this 'software engineering' thing sounds good...

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * It's the application of systematic approaches to developing software, in addition to developing the software itself 

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Term first coined by Margaret Hamilton, while leading the software effort on the Apollo missions 
#     * **in an attempt to get software practices taken more seriously**
# <img src="./hamilton1.jpg" style="display: block; margin-left: auto; margin-right: auto; width: 200px"> 

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Covers: requirements, design, tools, testing, deployment, documentation...

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## TL;DR 
# ### If software development is the art of putting bugs into code, software engineering is the art of taking them out

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * The majority of software engineering best practices are collaborative
#     * Peer reviews, documentation, issue tracking, readability/coding style, version control...    

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Many techniques rely on adopting particular collaborative working cultures
#     * Pair programming, code reuse, automation, requirements capture...

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * For a sole developer, most of these are out of reach

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## But I'm already No 1, so why try harder?
#
# <img src="./no1.png" style="display: block; margin-left: auto; margin-right: auto; width: 260px">

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * For a long time it's been possible to be a top researcher in the field, dependent on software for results without ever having shared working, reliable code.

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## Why try harder?

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Funders still give you money

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Publishers still publish without accompanying code (and data)

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Peer reviewers do not insist on having verifiable code

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Unlikely that anyone would ask to repeat your findings using your scripts (also easy to fob off)

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * You happily did a PhD without this sort of collaboration - the code probably wasn't reviewed, shared or run by anyone else

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## This is fine?
#
# * On the whole, people seemed fairly relaxed about this.
# <img src="./fine.png" style="display: block; margin-left: auto; margin-right: auto; width: 460px">
# * Until...**The Replication Crisis**

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## Crisis, what crisis?
#
# * Many findings were found to be doubtful, due to [1]:
#     * Generation of new data/publications at an unprecedented rate.
#     * Compelling evidence that the majority of these discoveries will not stand the test of time.
#     * **Causes: failure to adhere to good scientific practice and the desperation to publish or perish**
#     * This is a multifaceted, multistakeholder problem.
#     * No single party is solely responsible, and no single solution will suffice.
#     
# <small>1. wikipedia</small>

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * A lack of code sharing, knowledge of how to use programs & buggy code cited as issues.

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## Things are improving (slowly)
#
# * Papers are being written about replication in the field.

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Uptake of funders insisting on sharing research artefacts, using open science approaches
#     * Wellcome Trust guidelines - '...A suitable revision control system and issue tracker should be in place before programming work begins. This should be available for all members of the research team'

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Publishers starting to also show movements with registered reports, sharing code and data, open access schemes

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * The Systems Engineering Team keeps going on about something called 'git', and mentioning they maintain lots of collaboration tools...

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## Common excuses for not collaborating


# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * I don't share my code because it isn't good enough
#     * It won't get any better until others can see and use it
#     * If it's good enough to do a job, it's good enough to share 


# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * It's only for a 'one-off'
#     * Will a published result be dependent on the code being correct?
#     * How many colleagues are re-inventing this wheel?


# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * I won't be able to cope with the support requests
#     * Release and see what happens! Usually great effort is required to get your code noticed, let alone other users
#     * You don't have to support it


# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Someone will steal my awesome code
#     * Your code is probably bug-ridden and too specific for other use cases


# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## How do we take the bugs out?

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Use git - version control

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Peer review code

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Write and run tests

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Use an issue tracker

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Document - e.g. jupyter notebooks

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Automate - code as much of your process as possible

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Write down your process - then bugs in the process itself can be improved upon

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Use a consistent coding style (with static checker) - code readability is a large source of bugs

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## Code reviews
#
# * Are the most efficent, cheapest way of finding largest number of bugs according to some measurements 

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Average effectiveness of 'formal' code review is 60% compared to 40% for system testing, 35% for integration testing and 30% for unit testing (Code Complete 2nd Ed)
#     

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * A second person performing a code review will find different bugs, unlike a second test run
#     

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * These are 'formal' reviews - with a written checklist, but inherently pragmatic

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## Code review checklists
#
# * A checklist ensures only one type of issue is checked at a time

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * The checklist is compiled and reviewed regularly to identify the most bugs quickly, so checks for the most common types of bug first 
#     * hence no generic checklist exists, specialised to type of org/software

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Checklist designed to be run in 30-60 mins on 300-400 lines of code maximum
#     * To find as many bugs as possible before a reviewer gets fatigued or bored

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## In summary
# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * We'll never write good code (alone)
# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Bugs being put into code is an invariant
# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Software development is the art of putting bugs into code, software engineering is the art of taking bugs out
# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * It's the software engineering process that creates good software, more than the people 
# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * The best software engineering processes are collaborative
# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * Code reviews are the cheapest, most effective single first step to finding bugs

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * A change is occuring, adoption of these practices will become more important
