#! /usr/bin/env bash

# Takes one parameter, the notebook file to view


source /opt/anaconda3/etc/profile.d/conda.sh
JDIR=~/jupyter-notebooks
conda activate $JDIR

jupyter nbconvert --to slides "$1" --reveal-prefix ../reveal.js --output index
mv index.slides.html index.html
